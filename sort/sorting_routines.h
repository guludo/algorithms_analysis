#pragma once

#include <stdlib.h>

void insertion_sort(long *array, long *buffer, size_t n);
void selection_sort(long *array, long *buffer, size_t n);
void merge_sort(long *array, long *buffer, size_t n);
void quick_sort(long *array, long *buffer, size_t n);
void random_quick_sort(long *array, long *buffer, size_t n);
