#!/bin/python

from numpy import mean, std
import re
import sys

n_re = re.compile(r'n=(\d+)')
ticks_re = re.compile(r':ticks$')
chunk_re = re.compile(r'([^=]+)=(.+)')

fields_info = {
    't': {'type' : float},
    'ticks' : {'type' : int}
}
for f in fields_info:
    fields_info[f]['re'] = re.compile(":" + f + "$")

data = {}
fields_keys = []
n_list = []

def parse_field(chunk):
    m = chunk_re.fullmatch(chunk)
    if m is None: return None
    field_name = m.group(1)
    field_value_str = m.group(2)
    for f in fields_info:
        info = fields_info[f]
        if info['re'].search(field_name) is None: continue
        return field_name, info['type'](field_value_str)

    return None

def generate_dict(line):
    d = {}
    s = line.strip().split()
    for chunk in s:
        field = parse_field(chunk)
        if field is None: continue
        name, value = field
        d[name] = value
    return d

def append(n, line):
    d = generate_dict(line)

    if n not in data:
        data[n] = {}
        n_list.append(n)
    acc = data[n]

    for k in d:
        if k not in acc: acc[k] = []
        if k not in fields_keys: fields_keys.append(k)
        acc[k].append(d[k])

for line in sys.stdin:
    m = n_re.match(line)
    if m is None: continue
    append(int(m.group(1)), line)

print('"n" "' + '" "'.join(k for k in fields_keys) + '"')

n_list.sort()

for n in n_list:
    d = data[n]
    print(str(n) + " " + " ".join(str(mean(d[k])) for k in fields_keys))
