#pragma once

#include <stdio.h>

struct sort_routine {
    void (*sort)(long *array, long *buffer, size_t n);
    char name[512];
};

/*
 * struct size_sequence defines a sequence of uniformly distributed sizes
 * starting at start, with a variation of variation and with n elements.
 */
struct size_sequence {
    /* a valid size sequence must have start greater than zero. zero is used to
     * define the end of an array of struct size_sequence */
    size_t start;
    size_t variation;
    size_t n;
};

enum sort_array_type {
    SORT_DATA_TYPE_SORTED = 0, /* array is sorted */
    SORT_DATA_TYPE_ALMOST_SORTED, /* array is almost sorted */
    SORT_DATA_TYPE_RSORTED, /* array is reversely sorted */
    SORT_DATA_TYPE_RANDOM /* array is randomly sorted */
};

struct sort_array {
    enum sort_array_type type;
    long *array;
    size_t size;
};

extern size_t sort_array_ticks;

/* allocate memory and fill array */
int sort_array_init(struct sort_array *data,
                   enum sort_array_type type,
                   size_t size);

/* free memory */
int sort_array_destroy(struct sort_array *data);

int is_sorted(long *array, size_t n);

/* call sorting routines and output stats */
int sort_stats(struct size_sequence *size_sequences,
               struct sort_routine *sort_routines,
               struct sort_array *arrays,
               int num_calls,
               FILE *output);
