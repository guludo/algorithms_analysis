#/bin/bash

SCRIPT_DIR=$(dirname $(realpath ${BASH_SOURCE[0]}))

usage() {
    cat <<EOF >&$1
Usage: sort_stats_to_csv.sh STATS_FILE OUTPUT_DIR
EOF
}

[[ $# -ne 2 ]] && usage 2 && exit 1

stats_file="$1"
output_dir="$2"
algorithms="insertion_sort
            merge_sort
            quick_sort
            random_quick_sort
            selection_sort"

mkdir -p "$output_dir"

pids=

for a in $algorithms; do
    { cat "$stats_file" \
         | $SCRIPT_DIR/filter_sort_stats.sh -a $a \
         | $SCRIPT_DIR/process_sort_stats.py > "$output_dir/$a.txt" ; } &
    pids+=" $!"
done

wait $pids
