#include "sort.h"
#include "sorting_routines.h"

#define SORT_ARRAY_MAX_SIZE 1000000
#define NUM_CALLS 100

static struct size_sequence size_sequences[] = {
    {.start = 1, .variation = 1, .n = 9},
    {.start = 10, .variation = 10, .n = 9},
    {.start = 100, .variation = 100, .n = 9},
    {.start = 1000, .variation = 1000, .n = 9},
    {.start = 10000, .variation = 10000, .n = 9},
    {.start = 100000, .variation = 100000, .n = 1},
    {}
};

static struct sort_array sort_arrays[5] = {};
static struct sort_routine sort_routines[] = {
    {.sort = &insertion_sort, .name = "insertion_sort"},
    {.sort = &selection_sort, .name = "selection_sort"},
    {.sort = &merge_sort, .name = "merge_sort"},
    {.sort = &quick_sort, .name = "quick_sort"},
    {.sort = &random_quick_sort, .name = "random_quick_sort"},
    {.sort = NULL}
};

static int init_arrays()
{
    sort_array_init(sort_arrays,
                    SORT_DATA_TYPE_SORTED,
                    SORT_ARRAY_MAX_SIZE);
    sort_array_init(sort_arrays + 1,
                    SORT_DATA_TYPE_ALMOST_SORTED,
                    SORT_ARRAY_MAX_SIZE);
    sort_array_init(sort_arrays + 2,
                    SORT_DATA_TYPE_RSORTED,
                    SORT_ARRAY_MAX_SIZE);
    sort_array_init(sort_arrays + 3,
                    SORT_DATA_TYPE_RANDOM,
                    SORT_ARRAY_MAX_SIZE);
    return 0;
}

static int destroy_arrays()
{
    sort_array_destroy(sort_arrays);
    sort_array_destroy(sort_arrays + 1);
    sort_array_destroy(sort_arrays + 2);
    return 0;
}

int main(int argc, char *argv[])
{
    if (init_arrays() < 0)
        return -1;

    sort_stats(size_sequences, sort_routines, sort_arrays, NUM_CALLS, stdout);

    destroy_arrays();
    return 0;
}
