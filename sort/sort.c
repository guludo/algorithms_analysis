#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "sort.h"

size_t sort_array_ticks;

const char *array_type_names[] = {
    [SORT_DATA_TYPE_SORTED] = "sorted",
    [SORT_DATA_TYPE_ALMOST_SORTED] = "almost_sorted",
    [SORT_DATA_TYPE_RSORTED] = "reverse",
    [SORT_DATA_TYPE_RANDOM] = "random"
};

int sort_array_init(struct sort_array *data,
                   enum sort_array_type type,
                   size_t size)
{
    size_t i;

    data->array = malloc(sizeof(long) * size);
    if (data->array == NULL)
        return -1;

    data->type = type;
    data->size = size;

    for (i = 0; i < size; i++)
        switch (type) {
        case SORT_DATA_TYPE_SORTED:
        case SORT_DATA_TYPE_ALMOST_SORTED:
            data->array[i] = i;
            break;
        case SORT_DATA_TYPE_RSORTED:
            data->array[i] = size - 1 - i;
            break;
        case SORT_DATA_TYPE_RANDOM:
            data->array[i] = random();
            break;
        }

    if (type == SORT_DATA_TYPE_ALMOST_SORTED) {
        size_t interval = (size_t)(size * .1); /* 10% of shuffling */
        for (i = 0; i + interval < size; i += interval * 2) {
            long aux = data->array[i];
            data->array[i] = data->array[i + interval];
            data->array[i + interval] = aux;
        }
    }

    return 0;
}

int sort_array_destroy(struct sort_array *data)
{
    free(data->array);
    return 0;
}

int is_sorted(long *array, size_t n)
{
    size_t i;
    for (i = 1; i < n; i++)
        if (array[i - 1] > array[i])
            return 0;
    return 1;
}

int sort_stats(struct size_sequence *seq,
               struct sort_routine *routines,
               struct sort_array *arrays,
               int num_calls,
               FILE *output)
{
    long *working_copy, *working_buffer;
    int r = 0;
    size_t max_size = 0;
    size_t i, n, seq_max;

    /* find max_size */
    for (i = 0; arrays[i].size; i++)
        if (arrays[i].size > max_size)
            max_size = arrays[i].size;

    working_copy = malloc(2 * max_size * sizeof(long));
    if (working_copy == NULL)
        return -1;
    working_buffer = working_copy + max_size;

    n = seq->start;
    seq_max = seq->start + seq->n * seq->variation;
    while (1) {
        struct sort_routine *routine;
        int j;

        if (n > max_size)
            goto sort_stats_next_n;

        for (j = 0; j < num_calls; j++) {
            fprintf(output, "n=%lu ", n);

            for (routine = routines; routine->sort != NULL; routine++) {
                struct sort_array *a;
                for (a = arrays; a->size; a++) {
                    clock_t t;
                    double dt;
                    memcpy(working_copy, a->array, n * sizeof(long));
                    sort_array_ticks = 0;
                    t = clock();
                    routine->sort(working_copy, working_buffer, n);
                    dt = (double)(clock() - t) / CLOCKS_PER_SEC;

                    if (!is_sorted(working_copy, n)) {
                        r = -1;
                        fprintf(stderr,
                                "Oops, it looks like sorting routine %s is not actually sorting\n",
                                routine->name);
                        goto sort_stats_exit;
                    }

                    fprintf(output, "%s:%s:t=%lf %s:%s:ticks=%lu ",
                            routine->name, array_type_names[a->type], dt,
                            routine->name, array_type_names[a->type],
                            sort_array_ticks);
                }
            }
            fprintf(output, "\b\n");
        }

        fprintf(stderr, "\rn=%lu", n);
sort_stats_next_n:
        n += seq->variation;
        if (n == seq_max) {
            seq++;
            if (!seq->start)
                break;

            n = seq->start;
            seq_max = seq->start + seq->n * seq->variation;
        }
    }

sort_stats_exit:
    free(working_copy);
    return r;
}
