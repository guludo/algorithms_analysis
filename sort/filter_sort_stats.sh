#/bin/bash

set -e
declare -A filters_defaults
declare -A filters

filter_keys="algorithm array_type key"

filters_defaults=(
    [algorithm]="[^:]\+"
    [array_type]="[^:]\+"
    [key]="[^=]\+"
)
filters=(
    [algorithm]="${filters_defaults[algorithm]}"
    [array_type]="${filters_defaults[array_type]}"
    [key]="${filters_defaults[key]}"
)

usage() {
    cat <<EOF >&$1
Usage: filter_data.sh [OPTIONS]

Options:
    -a, --algorithm
    -t, --array_type
    -k, --key
    -h, --help
EOF
}

set_filter() {
    local filter=$1
    local value="$2"
    [[ "$value" == "*" ]] && value=${filters_defaults[$filter]}
    filters[$filter]="$value"
}

process() {
        grep -o -e "n=[^ ]\+" \
                -e "${filters[algorithm]}:${filters[array_type]}:${filters[key]}=[^ ]\+" \
            | tr "\n" " " \
            | sed "s/ n=/\nn=/g"
}

while (($# > 0)); do
    o="$1"
    case $o in
        -a|--algorithm)
            shift || true
            set_filter algorithm "$1"
            ;;
        -t|--array_type)
            shift || true
            set_filter array_type "$1"
            ;;
        -k|--key)
            shift || true
            set_filter key "$1"
            ;;
        -h|--help)
            usage 1
            exit 0
            ;;
        *)
            usage 2
            exit 1
            ;;
    esac
    shift
done

process
