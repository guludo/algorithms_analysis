#!/bin/bash

all_algorithms="insertion_sort
                selection_sort
                merge_sort
                quick_sort
                random_quick_sort"
squared_algorithms="insertion_sort
                    selection_sort
                    quick_sort"
efficient_algorithms="merge_sort
                      random_quick_sort"
keys="t ticks"
array_types="sorted
             almost_sorted
             reverse
             random"

declare -A keys_human_text
declare -A Keys_human_text
keys_human_text=(
    [t]="time in seconds"
    [ticks]="number of ticks"
)
Keys_human_text=(
    [t]="Time in seconds"
    [ticks]="Number of ticks"
)

usage() {
    cat <<EOF >&$1
Usage: plot_sort_stats_table.sh STATS_TABLE OUTPUT_DIR

STATS_TABLE: the path to the file generated with process_sort_stats.py
OUTPUT_DIR: folder to save the image files
EOF
}

[[ $# -ne 2 ]] && usage 2 && exit 1

STATS_FILE="$1"
OUTPUT_DIR="$2"

output_dir=

plot_config() {
    local output=$1
    cat <<EOF
set term png size 1280,1280
set output "$output_dir/$output.png"
set xlabel "N"
EOF
}

plot_title() {
    echo "set title \"$1\""
}

plot_ykey() {
    echo "set ylabel \"${Keys_human_text[$1]}\""
}

plot_start() {
    printf "plot "
}

plot_field() {
    local title=${2//_/ }
    printf "\"$STATS_FILE\" using \"n\":\"$1\":1 title \"$title\" with lines,"
}

plot_end() {
    :
}

make_common_plots() {
    output_dir=$OUTPUT_DIR
    for a in $all_algorithms; do
        for k in $keys; do
            {
                plot_config array_types-$a-$k
                plot_ykey $k
                plot_title "${Keys_human_text[$k]} for algorithm ${a//_/ }"
                plot_start
                for t in $array_types; do
                    plot_field $a:$t:$k $t
                done
                plot_end
            } | gnuplot
        done
    done

}

make_plots() {
    algorithms=$1
    output_dir=$2

    for t in $array_types; do
        for k in $keys; do
            {
                plot_config algorithms-$t-$k
                plot_ykey $k
                plot_title "${Keys_human_text[$k]} for array type ${t//_/ }"
                plot_start
                for a in $algorithms; do
                    plot_field $a:$t:$k $a
                done
                plot_end
            } | gnuplot
        done
    done
}

mkdir -p "$OUTPUT_DIR"

mkdir -p "$OUTPUT_DIR/all_algorithms"
make_plots "$all_algorithms" "$OUTPUT_DIR/all_algorithms"

mkdir -p "$OUTPUT_DIR/squared_algorithms"
make_plots "$squared_algorithms" "$OUTPUT_DIR/squared_algorithms"

mkdir -p "$OUTPUT_DIR/efficient_algorithms"
make_plots "$efficient_algorithms" "$OUTPUT_DIR/efficient_algorithms"

