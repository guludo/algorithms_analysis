#include "sorting_routines.h"
#include "sort.h"

#include <stdio.h>

void insertion_sort(long *array, long *buffer, size_t n)
{
    size_t j;
    for (j = 1; j < n; j++) {
        long key = array[j];
        ssize_t i = j - 1;
        while (i >= 0 && array[i] > key) {
            array[i + 1] = array[i];
            sort_array_ticks++;
            i--;
        }
        array[i + 1] = key;
        sort_array_ticks++;
    }
}

void selection_sort(long *array, long *buffer, size_t n)
{
    size_t l = n - 1;
    size_t j;
    for (j = 0; j < l; j++) {
        size_t smallest_idx = j;
        size_t i;
        long aux;
        for (i = j + 1; i < n; i++) {
            if (array[i] < array[smallest_idx])
                smallest_idx = i;
        }
        aux = array[smallest_idx];
        array[smallest_idx] = array[j];
        sort_array_ticks++;
        array[j] = aux;
        sort_array_ticks++;
    }
}

void merge_sort_engine(long *array, long *buffer, size_t p, size_t r)
{
    size_t i, j, q;
    if (p >= r)
        return;

    q = (p + r) / 2;
    merge_sort_engine(array, buffer, p, q);
    merge_sort_engine(array, buffer, q + 1, r);

    /* merging */
    for (i = p; i <= q; i++) {
        buffer[i] = array[i];
        sort_array_ticks++;
    }
    for (i = q + 1; i <= r; i++) {
        buffer[r + q + 1 - i] = array[i];
        sort_array_ticks++;
    }

    i = p;
    j = r;
    for (q = p; q <= r; q++) {
        if (buffer[i] <= buffer[j]) {
            array[q] = buffer[i++];
            sort_array_ticks++;
        } else {
            array[q] = buffer[j--];
            sort_array_ticks++;
        }
    }
}

void merge_sort(long *array, long *buffer, size_t n)
{
    merge_sort_engine(array, buffer, 0, n - 1);
}


static ssize_t quick_sort_partition(long *array, ssize_t p, ssize_t r)
{
    long pivot = array[r];
    long aux;
    ssize_t i = p - 1;
    ssize_t j;
    for (j = p; j < r; j++) {
        if (array[j] < pivot) {
            aux = array[++i];
            array[i] = array[j];
            sort_array_ticks++;
            array[j] = aux;
            sort_array_ticks++;
        }
    }
    aux = array[++i];
    array[i] = array[r];
    sort_array_ticks++;
    array[r] = aux;
    sort_array_ticks++;
    return i;
}

static void quick_sort_engine(long *array, ssize_t p, ssize_t r)
{
    ssize_t q;
    if (p >= r)
        return;

    q = quick_sort_partition(array, p, r);
    quick_sort_engine(array, p, q - 1);
    quick_sort_engine(array, q + 1, r);
}

void quick_sort(long *array, long *buffer, size_t n)
{
    quick_sort_engine(array, 0, n - 1);
}

static ssize_t random_quick_sort_partition(long *array, ssize_t p, ssize_t r)
{
    ssize_t i = p + (size_t)((double)random() / RAND_MAX * (r - p));
    long aux = array[i];
    array[i] = array[r];
    sort_array_ticks++;
    array[r] = aux;
    sort_array_ticks++;
    return quick_sort_partition(array, p, r);
}

static void random_quick_sort_engine(long *array, ssize_t p, ssize_t r)
{
    ssize_t q;
    if (p >= r)
        return;

    q = random_quick_sort_partition(array, p, r);
    random_quick_sort_engine(array, p, q - 1);
    random_quick_sort_engine(array, q + 1, r);
}

void random_quick_sort(long *array, long *buffer, size_t n)
{
    random_quick_sort_engine(array, 0, n - 1);
}
